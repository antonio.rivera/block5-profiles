package com.example.block5profiles;

import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.ResourcePropertySource;

@SpringBootApplication
public class Block5ProfilesApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Block5ProfilesApplication.class, args);
	}


	@Autowired
	private ConfigurableApplicationContext context;
	@Override
	public void run(String... args) throws Exception {
		ConfigurableEnvironment env = context.getEnvironment();


		String activeProfile = env.getActiveProfiles()[0];
		System.out.println("El perfil activo es: " + activeProfile);

		PropertySource ps = new ResourcePropertySource("classpath:" + activeProfile + ".properties");
		env.getPropertySources().addFirst(ps);
		String dbUrl = env.getProperty("bd.url");
		System.out.println("La URL de la base de datos es: " + dbUrl);


	}
}
